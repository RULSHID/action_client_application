Action-client server application


It is  a  python2.7 based program in ROS  environment using action client server  in which the system takes a person's name as input  and display his name by searching it in the  predefined dictionary . 


For example, If the user gives “rulshid”  as input then the output of the system will be :22  i.e corresponding age

 


You need to  install ros in your Linux os ( i’m using ubuntu 16.04) by using this link: http://wiki.ros.org/kinetic/Installation/Ubuntu

Requirements:

1.Pip
Run the following command to update the package list and upgrade all of your system software to the latest version available

 sudo apt-get update && sudo apt-get -y upgrade

then install pip
 
 sudo apt-get install python-pip

2.PyAudio :

 sudo apt-get install portaudio19-dev python-all-dev python3-all-dev && sudo pip install pyaudio            
                                                                                                                                 
3.SpeechRecognition 3.8.1:

 pip install SpeechRecognition

 Follow this link for more deatils:https://pypi.org/project/SpeechRecognition/


After installing all the above things
 
  Follow these steps:

1. Create a catkin workspace

 Follow the link: http://wiki.ros.org/catkin/Tutorials/create_a_workspace

2.Create a catkin Package

 Follow the link: http://wiki.ros.org/ROS/Tutorials/CreatingPackage

3.Create Action Client

 Follow the link:http://wiki.ros.org/actionlib_tutorials/Tutorials/Writing%20a%20Simple%20Action%20Client%20%28Python%29

