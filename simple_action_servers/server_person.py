#! /usr/bin/env python
##Python 2.x program
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
##Copyright (c) 2018, MUHAMMED RULSHID.S, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License

import rospy
# Brings in the SimpleActionClient
import actionlib
# Brings in the messages used by the person action, including the
# goal message and the result message.
import action_client_application.msg


class PersonAction(object):
    # create messages that are used to publish feedback/result
    _feedback = action_client_application.msg.PersonFeedback()
    _result = action_client_application.msg.PersonResult()
    #SimpleActionServer is created
    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, action_client_application.msg.PersonAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()
    #execute callback function that we'll run everytime a new goal is received  
    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = True
        
        
        # let the user know that the action is executing. 
        rospy.loginfo('searching the details of  %s',goal.name)
        name=goal.name
       
        # check that preempt has not been requested by the client
        if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
        #create dictionary containing persons age     
        person_details={'rulshid': 22, 'raju':50, 'sabith':49, 'radha': 25}
        f=0
        ## start executing the action
        for i in person_details:

            if i==name:
                age=person_details[i]
                f=1  #flag
                self._feedback.sequence= age
                self._as.publish_feedback(self._feedback) 
                break
            if f=0:
                self._feedback.sequence= 0
                self._as.publish_feedback(self._feedback) 
                
                    
        # this step is not necessary, the sequence is computed at 1 Hz for demonstration purposes
        r.sleep()
          
        if success:
            #action server notifies the action client that the goal is complete
            self._result.sequence = self._feedback.sequence
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)

#start of main function        
if __name__ == '__main__':
    
    rospy.init_node('details')
    # creates the action server
    server = PersonAction(rospy.get_name())
    rospy.spin()
