#! /usr/bin/env python
##Python 2.x program
#With in the environment
#declare -x ROSLISP_PACKAGE_DIRECTORIES=""
#declare -x ROS_DISTRO="kinetic"
#declare -x ROS_ETC_DIR="/opt/ros/kinetic/etc/ros"
#declare -x ROS_MASTER_URI="http://localhost:11311"
#declare -x ROS_PACKAGE_PATH="/opt/ros/kinetic/share"
#declare -x ROS_ROOT="/opt/ros/kinetic/share/ros"
#declare -x ROS_VERSION="1"
##Copyright (c) 2018, MUHAMMED RULSHID.S, Licensed under the
#Educational Community License, Version 2.0 (the "License"); you may
#not use this file except in compliance with the License. You may
#obtain a copy of the License at

#http://www.osedu.org/licenses/ECL-2.0

#Unless required by applicable law or agreed to in writing,
#software distributed under the License is distributed on an "AS IS"
#BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
#or implied. See the License for the specific language governing
#permissions and limitations under the License
from __future__ import print_function
import rospy
# Brings in the SimpleActionClient
import actionlib
import sys

# Brings in the messages used by the person action, including the
# goal message and the result message.
import action_client_application.msg

def person_client(y):
    # Creates the SimpleActionClient, passing the type of the action
    # (PersonAction) to the constructor.
    client = actionlib.SimpleActionClient('details', action_client_application.msg.PersonAction)
    
    # Waits until the action server has started up and started
    # listening for goals.
    client.wait_for_server()
    
    # Creates a goal to send to the action server.
    goal = action_client_application.msg.PersonGoal(name=y)
    # Sends the goal to the action server.
    client.send_goal(goal)
    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result() 
#start of main function
if __name__ == '__main__':
 
 while 1:
    
    try:
        y=raw_input("Enter the name of a person  ")
        print(y)
        # Initializes a rospy node so that the SimpleActionClient can
        # publish and subscribe over ROS.
        rospy.init_node('person_client')
        result = person_client(y)
        if result.sequence==0:
            print 'no match found'
        else:
            print("name:",y)
            print("Age:",result.sequence)
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)

